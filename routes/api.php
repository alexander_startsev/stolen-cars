<?php

use App\Http\Controllers\MakerModelsController;
use App\Http\Controllers\StolenCarsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('car', [StolenCarsController::class, 'index'])->name('api.car.index');
Route::get('car/export', [StolenCarsController::class, 'export'])->name('api.car.export');
Route::post('car', [StolenCarsController::class, 'store'])->name('api.car.store');
Route::put('car/{car}', [StolenCarsController::class, 'update'])->name('api.car.update');
Route::delete('car/{car}', [StolenCarsController::class, 'destroy'])->name('api.car.destroy');

Route::get('models', [MakerModelsController::class, 'index'])->name('api.model.index');
