<?php

namespace App\Models;

use App\Traits\Syncable;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    use Syncable;

    protected $fillable = ['car_maker_id', 'model'];

    protected static $syncKey = 'model_id';
    protected static $syncMap = [
        'name' => 'Model_Name',
        'model_id' => 'Model_ID'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:i',
        'updated_at' => 'datetime:Y-m-d H:m:i'
    ];

    public function maker()
    {
        return $this->belongsTo(CarMaker::class);
    }
}
