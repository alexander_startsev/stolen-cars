<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StolenCar extends Model
{
    protected $fillable = ['name', 'number', 'color', 'vin', 'producer', 'model', 'year'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:i',
        'updated_at' => 'datetime:Y-m-d H:m:i'
    ];

    const PAGINATION_LIMIT = 1;

    public function scopeFilter($query, $request)
    {
        $query
            ->when($request->has('order_by'),
                function ($query) use ($request) {
                    return $query->orderBy($request->get('order_by'));
                }
            )
            ->when($request->has('producer'),
                function ($query) use ($request) {
                    return $query->where('producer', $request->get('producer'));
                }
            )
            ->when($request->has('model'),
                function ($query) use ($request) {
                    return $query->where('model', $request->get('model'));
                }
            )
            ->when($request->has('model_year'),
                function ($query) use ($request) {
                    return $query->where('year', $request->get('model_year'));
                }
            )
            ->when($request->has('search'),
                function ($query) use ($request) {
                    $search = $request->get('search');

                    return $query->where(function ($q) use ($search) {
                        $q->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('number', 'LIKE', '%' . $search . '%')
                            ->orWhere('vin', 'LIKE', '%' . $search . '%');
                    }
                );
            });

        return $query;
    }
}
