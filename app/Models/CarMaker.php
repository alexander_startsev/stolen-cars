<?php

namespace App\Models;

use App\Traits\Syncable;
use Illuminate\Database\Eloquent\Model;

class CarMaker extends Model
{
    use Syncable;

    protected $fillable = ['make_id', 'name'];

    protected static $syncKey = 'make_id';
    protected static $syncMap = [
        'make_id' => 'Make_ID',
        'name' => 'Make_Name'
    ];

    public function models()
    {
        return $this->hasMany(CarModel::class);
    }
}
