<?php

namespace App\Services;

use App\Http\Request\StolenCarListRequest;
use App\Models\StolenCar;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class StolenCarExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    protected $request = null;

    public function filterByRequest(StolenCarListRequest $request): StolenCarExport
    {
        $this->request = $request;

        return $this;
    }

    public function query()
    {
        $request = $this->request;

        return StolenCar::query()->when($request, function ($query) use ($request) {
            return $query->filter($request);
        });
    }

    public function headings(): array
    {
        return [
            'Name',
            'Number',
            'Color',
            'VIN',
            'Manufacturer',
            'Model',
            "Year"
        ];
    }

    public function map($car): array
    {
        return [
            $car->name,
            $car->number,
            $car->color,
            $car->vin,
            $car->producer,
            $car->model,
            $car->year
        ];
    }
}
