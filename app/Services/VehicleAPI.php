<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use RuntimeException;

class VehicleAPI
{
    const URL = 'https://vpic.nhtsa.dot.gov/api';

    public static function decodeVIN($vin)
    {
        $response = Http::get(self::URL . '/vehicles/decodevinvalues/' .$vin, [
            'format' => 'json'
        ]);

        return $response->json();
    }

    public static function getMakersList()
    {
        $response = Http::get(self::URL . '/vehicles/getallmakes', [
                'format' => 'json'
            ]
        );

        if ($response->failed())
            throw new RuntimeException("Failed to connect ", $response->status());

        $data = $response->json();

        return $data['Results'];
    }

    public static function getCarModelsByMakerId($id)
    {
        $response = Http::get(self::URL . '/vehicles/getmodelsformakeid/' . $id, [
                'format' => 'json'
            ]
        );

        if ($response->failed())
            throw new RuntimeException("Failed to connect ", $response->status());

        $data = $response->json();

        return $data['Results'];
    }
}
