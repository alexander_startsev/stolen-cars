<?php

namespace App\Services;

use App\Jobs\ImportMakerModels;
use App\Models\CarMaker;
use App\Models\CarModel;

class VehicleImport
{
    protected $makers;

    public function import()
    {
        $this->importMakers();
        $this->importModels();
    }

    protected function importMakers()
    {
        $this->makers = VehicleAPI::getMakersList();

        if (empty($this->makers)) return false;

        return CarMaker::sync($this->makers);
    }

    protected function importModels()
    {
        if (empty($this->makers)) return false;

        foreach ($this->makers as $maker) {
            ImportMakerModels::dispatch($maker['Make_ID']);
        }
    }

    public function importModelsByMakeID($makeId)
    {
        $models = VehicleAPI::getCarModelsByMakerId($makeId);

        if (empty($models)) return false;

        $maker = CarMaker::where('make_id', $makeId)->first();

        return CarModel::sync($models, [
            'relation' => [
                'key' => 'car_maker_id',
                'value' => $maker->id
            ]
        ]);
    }
}
