<?php

namespace App\Http\Controllers;

use App\Http\Request\MakerModelsRequest;
use App\Models\CarMaker;
use Illuminate\Http\JsonResponse;

class MakerModelsController extends ApiController
{
    public function index(MakerModelsRequest $request): JsonResponse
    {
        $maker = CarMaker::where('name', 'LIKE', '%' . $request->get('maker') . '%')
            ->with('models')
            ->first();

        return $this->sendSuccess($maker->models);
    }
}
