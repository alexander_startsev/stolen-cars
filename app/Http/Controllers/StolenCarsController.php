<?php

namespace App\Http\Controllers;

use App\Http\Request\StolenCarListRequest;
use App\Http\Request\StolenCarRequest;
use App\Http\Request\StolenCarUpdateRequest;
use App\Models\StolenCar;
use App\Services\StolenCarExport;
use App\Services\VehicleAPI;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Excel;

class StolenCarsController extends ApiController
{
    public function index(StolenCarListRequest $request): JsonResponse
    {
        $data = StolenCar::filter($request)
            ->when($request->has('limit'),
                function ($query) use ($request) {
                    return $query->limit($request->get('limit'));
                }, function ($query) {
                    return $query->limit(StolenCar::PAGINATION_LIMIT);
                }
            )
            ->when($request->has('page'),
                function ($query) use ($request) {
                    $offset = ($request->get('page') - 1) * StolenCar::PAGINATION_LIMIT;
                    return $query->offset($offset);
                }
            )
            ->get();

        return $this->sendSuccess($data);
    }

    public function store(StolenCarRequest $request): JsonResponse
    {
        $vin = $request->post('vin');
        $vinDecodedData = VehicleAPI::decodeVIN($vin);

        $carData = array_merge($request->validated(), [
            'producer' => $vinDecodedData['Results'][0]['Make'],
            'model' => $vinDecodedData['Results'][0]['Model'],
            'year' => $vinDecodedData['Results'][0]['ModelYear']
        ]);

        StolenCar::create($carData);

        return $this->sendSuccess($carData);
    }

    public function update(StolenCar $car, StolenCarUpdateRequest $request): JsonResponse
    {
        $data = $request->only((new StolenCar())->getFillable());
        if ($request->has('vin')) {
            $vin = $request->post('vin');
            $data = array_merge($data, VehicleAPI::decodeVIN($vin));
        }

        $car->update($data);

        return $this->sendSuccess($car->refresh());
    }

    public function export(StolenCarExport $export, StolenCarListRequest $request)
    {
        return $export
            ->filterByRequest($request)
            ->download('stolen-cars.xls', Excel::XLS, [
                'Content-Type' => 'application/vnd.ms-excel'
            ]);
    }

    public function destroy(StolenCar $car): JsonResponse
    {
        $car->delete();

        return $this->sendSuccess($car);
    }
}
