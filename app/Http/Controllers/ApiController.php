<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="Laravel Demo API Documentation",
 *     description="L5 Swagger OpenApi description",
 *     @OA\Contact(
 *         email="admin@mail.com"
 *     ),
 * )
 *
 * @OA\Tag (
 *     name="Stolen cars",
 *     description="Manage stolen cars"
 * )
 */
class ApiController
{
    use ApiResponser;
}
