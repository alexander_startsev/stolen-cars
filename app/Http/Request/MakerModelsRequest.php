<?php

namespace App\Http\Request;

class MakerModelsRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'maker' => 'required'
        ];
    }
}
