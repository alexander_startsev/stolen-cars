<?php

namespace App\Http\Request;

use Illuminate\Validation\Rule;

class StolenCarListRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'order_by' => [Rule::in(['name', 'number', 'color', 'vin', 'producer', 'model', 'year'])]
        ];
    }
}
