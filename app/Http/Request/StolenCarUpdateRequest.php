<?php

namespace App\Http\Request;

class StolenCarUpdateRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'vin' => 'regex:/^[a-zA-Z-0-9]{17}$/i'
        ];
    }
}
