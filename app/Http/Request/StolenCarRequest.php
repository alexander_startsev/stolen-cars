<?php

namespace App\Http\Request;

class StolenCarRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'number' => 'required',
            'color' => 'required',
            'vin' => 'required|regex:/^[a-zA-Z-0-9]{17}$/i'
        ];
    }
}
