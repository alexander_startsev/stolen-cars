<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

trait Syncable
{
    public function getHashAttribute()
    {
        $strForHash = '';
        foreach (self::$syncMap as $key => $field) {
            $strForHash .= $this->attributes[$key];
        }

        return hash('md5', $strForHash);
    }

    public static function sync(array $newData, array $options = [])
    {
        if (empty(self::$syncMap) || empty(self::$syncKey))
            throw new \Exception('Need set syncMap or syncKey for model');

        $exData = self::getExistedData($options);
        $mappedData = self::mapSyncedData($newData, $exData, $options);

        if (!empty($mappedData['toInsert'])) self::insert($mappedData['toInsert']);
        if (!empty($mappedData['toUpdate'])) self::batchUpdate($mappedData['toUpdate']);
        if (!empty($mappedData['toDelete'])) self::deleteOutdatedData($mappedData['toDelete']);

        return true;
    }

    protected static function batchUpdate(array $data)
    {
        $table = (new self())->getTable();

        $dataToUpdate = array();
        foreach ($data as $datum) {
            $syncKeyValue = $datum[self::$syncKey];

            foreach (self::$syncMap as $key => $field) {
                if ($key == self::$syncKey) continue;

                $dataToUpdate[$key][] = "WHEN " . self::$syncKey . " = '{$syncKeyValue}' THEN '{$datum[$key]}' ";
            }
        }

        $sql = "UPDATE {$table} SET ";

        foreach ($dataToUpdate as $key => $values) {
            $sql .= "{$key} = CASE " . implode(' ', $values) . " ELSE {$key} END";
        }

        DB::statement($sql);
    }

    protected static function mapSyncedData(array $newData, Collection $exData, array $options): array
    {
        $syncImportKey = self::syncKeyInImportData();
        $data = array();

        foreach ($newData as $i => $datum) {
            $syncKeyValue = $datum[$syncImportKey];
            self::replaysArrayKeyForMapping($newData, $i, 'sync_' . $syncKeyValue);
            $newRow = self::getSyncedFields($datum);

            if (!$exData->has($syncKeyValue)) {
                $data['toInsert'][] = self::completeRelation($newRow, $options);
            } else {
                $exDatum = $exData->get($syncKeyValue);
                if ($exDatum->hash !== self::importDataHash($datum))
                    $data['toUpdate'][] = $newRow;
            }
        }

        foreach ($exData as $key => $exDatum) {
            if (!isset($newData['sync_' . $key])) $data['toDelete'][] = $key;
        }

        return $data;
    }

    protected static function importDataHash(array $data)
    {
        $strForHash = '';
        foreach (self::$syncMap as $field) {
            $strForHash .= $data[$field];
        }

        return hash('md5', $strForHash);
    }

    protected static function getExistedData(array $options)
    {
        return self::when(isset($options['relation']),
            function ($query) use ($options) {
                return $query->where($options['relation']['key'], $options['relation']['value']);
            })
            ->get(array_keys(self::$syncMap))
            ->keyBy(self::$syncKey);
    }

    protected static function deleteOutdatedData(array $keysToDelete)
    {
        self::whereIn(self::$syncKey, $keysToDelete)
            ->delete();
    }

    protected static function getSyncedFields(array $datum): array
    {
        $data = array();
        foreach (self::$syncMap as $key => $field) {
            $data[$key] = $datum[$field];
        }

        return $data;
    }

    protected static function completeRelation(array $data, array $options): array
    {
        if (!isset($options['relation'])) return $data;

        return array_merge($data, [
            $options['relation']['key'] => $options['relation']['value']
        ]);
    }

    protected static function replaysArrayKeyForMapping(&$data, $oldKey, $newKey)
    {
        $data[$newKey] = $data[$oldKey];
        unset($data[$oldKey]);
    }

    protected static function syncKeyInImportData(): string
    {
        return self::$syncMap[self::$syncKey];
    }
}
