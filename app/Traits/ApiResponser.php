<?php

namespace App\Traits;

trait ApiResponser
{
    protected function sendSuccess($data, $code = 200, $message = null)
    {
        return response()->json([
            'status'=> 'Success',
            'message' => $message,
            'data' => $data
        ], $code);
    }

    protected function sendError($code, $message = null, $data = null)
    {
        return response()->json([
            'status'=>'Error',
            'message' => $message,
            'data' => $data
        ], $code);
    }
}
