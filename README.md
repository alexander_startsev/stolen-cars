# Разворачивание на локалке

```bash
$ cp .env.example .env
$ git config core.filemode false
$ sudo chmod -R 777 bootstrap
$ sudo chmod -R 777 storage
```

Заполнить параметры среды (.env)

```bash
$ docker-compose up -d --build
$ docker-compose exec app bash
$ composer install
$ php artisan key:generate
$ php artisan storage:link
$ php artisan migrate
```

Для тестирования функционала связанного с очередями использовать команду:
```bash
$ php artisan queue:listen
```

Команда для запуска импорта:
```bash
$ php artisan import:vehicle
```

Запуск планировщика:
```bash
$ php artisan schedule:work
```

Postman collection https://www.postman.com/collections/125e3fe7b0b847e01a51
